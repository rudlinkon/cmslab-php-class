<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Ppp{
    public $public;
    protected $protected;
    private $private;
    
    public function __construct($first='', $second='Protected property', $third='Private property') {
        $this->public = $first;
        $this->protected = $second;
        $this->private = $third;
    }
    
    public function getPrivate(){
        return $this->private;
    }
    
}

class InhPpp extends Ppp{
    public function getProtected(){
        return $this->protected;
    }
    

}

$first = new Ppp('Public property');

echo $first->getPrivate();

$second = new InhPpp;

//echo $second->getPrivate();

print_r($first);
print_r($second);