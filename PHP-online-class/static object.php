<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Student{
    var $name;
    var $roll;
    static $class = 'nine';
    const SCHOOLNAME = 'cmslab school';
    
    public static function staticmethod(){
        echo 'this is from static method';
    }
}

$sk = new Student;


echo Student::SCHOOLNAME;
echo '<br>';
Student::$class = 'ten';
echo Student::$class;
echo '<br>';
Student::staticmethod();