<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ParentClass{
    protected $jomi;
    
    private function setJomi($jomi){
        $this->jomi = $jomi;
    }
    
    public function getJomi(){
        return $this->jomi;
    }
}

class ChildClass extends ParentClass{
    public $ordhekjomi;
    
}

class GrandChildClass extends ChildClass{
    public $quarterjom;
}

$obj = new GrandChildClass;

$obj->setJomi(50);

echo $obj->getJomi();