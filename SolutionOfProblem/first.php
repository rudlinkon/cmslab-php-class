<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//first prog start
for($i=1; $i<=10; $i++){
    $arr[] = $i;
}
echo implode('-',$arr);
//first prog end

echo '<br>';

//second prog start 
$result=0;
for($i=0; $i<=30; $i++){
    $result += $i;
}
echo $result;
//second prog end

echo '<br>';

//3rd prog start
for($i=1; $i<=5; $i++){
    for($j=1; $j<=$i; $j++){
        echo '*';
    }
    echo '<br>';
}
//3rd prog end

echo '<br>';

//4th prog start
for($i=0; $i<10; $i++){
    for($j=0; $j<10; $j++){
        echo $i.$j.',';
    }
    echo $i%2!=0 ? '<br>' : null;//you can also use if( $i%2!=0 ){ echo '<br>';}
}
//4th prog end

echo '<br>';

//5th prog start
for($i=1; $i<=50; $i++){
    if( $i%3==0 && $i%5==0 ){
        echo 'BitmBasis ';
    }elseif( $i%3==0 ){
        echo 'Bitm ';
    }elseif( $i%5==0 ){
        echo 'Basis ';
    }else{
        echo $i.' ';
    }
}
//5th prog end